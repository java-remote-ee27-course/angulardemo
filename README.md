# AngularDemo

This is my second Angular project (in development at the moment). See the [Gitlab page](https://java-remote-ee27-course.gitlab.io/angulardemo)

## Pages 
- home - uses Angular Material mat-cards (one card as a template (matcard component), of what other cards are created using *ngFor). Image paths, descriptions etc. are loaded from "mock-content.ts". It uses interface "IPicture.ts". You can like pictures. The likes are saved in local storage, using methods in LocalService (local.storage.ts). All likes can be deleted by pressing "clear" button.
- about - as it runs on fake-json-server, the content is only loaded when you git clone project, install angular and globally a json server and run json server on your own computer. It uses customized re-usable class-cards and button components. Training times can be removed by clicking on X-s. Trainings can be added through Add form (see images below). Notifications can be set by double-cklicking cards or through the Add form. Page is still in development.
- gallery - uses Angular Material mat-card where authors and images are generated with *ngFor. Uses UI Core template for footer which does not work as excpected yet (a partial "see-through" footer)

![src/assets/home_08.11.23.png](src/assets/home_08.11.23.png)

![src/assets/gallery_28.10.23.png](src/assets/gallery_28.10.23.png)

![src/assets/about_08.11.23.png](src/assets/about_08.11.23.png)

![src/assets/about_with_add_08.11.23.png](src/assets/about_with_add_08.11.23.png)