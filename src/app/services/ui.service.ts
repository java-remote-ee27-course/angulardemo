import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UiService {
  private showAddClass: boolean = false;
  private subject = new Subject<any>();

  constructor() { }
  toggleAddClass(): void{
    //console.log(123);
    this.showAddClass = !this.showAddClass;
    this.subject.next(this.showAddClass);
  }

  onToggle(): Observable<any>{
    return this.subject.asObservable();
  }
}
