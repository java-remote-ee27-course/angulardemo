import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IClass } from "models/IClass";
const httpOptions = {
  headers: new HttpHeaders({
    'Content-type': 'application/json'
  })
}

@Injectable({
  providedIn: 'root'
})
export class ClassService {
  private apiUrl = "http://localhost:3000/classes";
  constructor(private http:HttpClient) { }

  getClasses(): Observable<IClass[]>{
    
    return this.http.get<IClass[]>(this.apiUrl);
  }
  
  //deletes from server and in classes.component.ts it filters them out from UI
  deleteClass(iClass: IClass): Observable<IClass>{
    const url = `${this.apiUrl}/${iClass.id}`;
    return this.http.delete<IClass>(url);
  }

  updateReminder(iClass: IClass): Observable<IClass>{
    const url = `${this.apiUrl}/${iClass.id}`;
    return this.http.put<IClass>(url, iClass, httpOptions);
  }

  addClass(iClass: IClass): Observable<IClass>{
    return this.http.post<IClass>(this.apiUrl, iClass, httpOptions);
  }
}
