import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalService {

  constructor() { }

  public saveData(key: string, value: string) {
    localStorage.setItem(key, value);
  }

  public getData(key: string) {
    return localStorage.getItem(key)
  }

  public parseData(key: string): number{
    try{ 
      return JSON.parse(localStorage.getItem(key)!);
    } catch(ex){
        console.log(ex)
        return 0;
    } 
  }

  public removeData(key: string) {
    localStorage.removeItem(key);
    location.reload();
  }

  public clearData() {
    localStorage.clear();
  }
}
