import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FooterModule } from '@coreui/angular';
import { MaterialModule } from './components/material/material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IconModule, IconSetService } from '@coreui/icons-angular';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { HomeComponent } from './components/home/home.component';
import { FooterComponent } from './components/footer/footer.component';
import { MatcardComponent } from './components/matcard/matcard.component';
import { MatcardMultipleImgComponent } from './components/matcard-multiple-img/matcard-multiple-img.component';
import { GalleryComponent } from './components/gallery/gallery.component';
import { AboutComponent } from './components/about/about.component';
import { ButtonComponent } from './components/button/button.component';
import { ClassesComponent } from './components/classes/classes.component';
import { ClassItemComponent } from './components/class-item/class-item.component';
import { MatcardsComponent } from './components/matcards/matcards.component';
import { AddClassComponent } from './components/add-class/add-class.component';




@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        FooterComponent,
        HomeComponent,
        MatcardComponent,
        MatcardMultipleImgComponent,
        GalleryComponent,
        AboutComponent,
        ButtonComponent,
        ClassesComponent,
        ClassItemComponent,
        MatcardsComponent,
        AddClassComponent,
    ],
    providers: [
      IconSetService,
    ],
    bootstrap: [AppComponent],
    imports: [
        BrowserModule,
        FormsModule,
        BrowserAnimationsModule,
        FooterModule,
        IconModule,
        MaterialModule,
        AppRoutingModule,
        FontAwesomeModule,
        HttpClientModule,
      ]
})
export class AppModule { }
