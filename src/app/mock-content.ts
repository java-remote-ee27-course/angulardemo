import { IPicture } from "models/IPicture";

let description1: string = `Likes are saved in local storage, so they remain after refresh. 
"Clear" deletes particular local storage item and refreshes page. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam. This is a dancer demo image.`;

let description2: string = `This is another dancer demo image. At vero eos et accusamus et iusto odio dignissimos ducimus 
qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et   quas molestias excepturi sint occaecati 
cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.`;

let description3: string = `This is yet another dancer demo image. Ed ut perspiciatis unde omnis iste natus error sit voluptatem 
accusantium doloremque laudantium,   totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto 
beatae vitae dicta sunt explicabo. `;

export const CARDCONTENT: IPicture[] = [
    { 
        id: 1,
        title: 'Electro', 
        path: 'assets/ease-3134828_640.jpg', 
        description: description1, 
        author: 'Janisos', 
        source: '(Image from Pixabay.)', 
        alt: 'Dancer image 1' 
    },
    { 
        id: 2,
        title: 'Hiphop', 
        path: 'assets/dance-1566852_640.jpg', 
        description: description2, 
        author: 'Gerd Altmann', 
        source: '(Image from Pixabay.)', 
        alt: 'Dancer image 2'
    },
    { 
        id: 3,
        title: 'Freedom', 
        path: 'assets/freedom-2940655_640.jpg', 
        description: description3, 
        author: 'Myriams-Fotos', 
        source: '(Image from Pixabay.)', 
        alt: 'Dancer image 3'
     }
]
