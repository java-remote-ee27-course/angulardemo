import { Component, Input, Output, EventEmitter } from '@angular/core';
import { IPicture } from 'models/IPicture';
import { LocalService } from '../../services/local.service';

// See: https://jscrambler.com/blog/working-with-angular-local-storage
@Component({
  selector: 'app-matcard',
  templateUrl: './matcard.component.html',
  styleUrls: ['./matcard.component.css'],  
})
export class MatcardComponent {
  @Input() background:string = '';
  @Input() iPicture: IPicture = {
    id: 0,
    title:'',
    path:'',
    description:'',
    author:'',
    source:'',
    alt:''
  }
  
  likeCounter : number  = 0;

  //Use local.service.ts 
  constructor(private localStoreService: LocalService){}

  //on initialize check local storage:
  ngOnInit():void{
    let key = JSON.stringify(this.iPicture.id);
    this.likeCounter = this.localStoreService.parseData(key);
  }

  onMatLikeBtnClick(){
    this.likeCounter++;
    let key = JSON.stringify(this.iPicture.id);
    let counter = JSON.stringify(this.likeCounter);
    this.localStoreService.saveData(key, counter);
  }

  onMatClearBtnClick(){
    let key = JSON.stringify(this.iPicture.id);
    this.localStoreService.removeData(key);
  }
}
