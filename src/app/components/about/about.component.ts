import { Component } from '@angular/core';
import { UiService } from 'src/app/services/ui.service';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})

export class AboutComponent {
  title:string = "Courses";
  showAddClass : boolean = false;
  subscription!: Subscription;
  /** 
  description1: string = `---This is a dancer demo image. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
  eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
  laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit.`;

  matcard : MatcardComponent = new MatcardComponent;
  */
  constructor(private uiService: UiService){
    this.subscription = this.uiService
      .onToggle()
      .subscribe((value) => (this.showAddClass = value));
  }

  toggleAddClass(){
    this.uiService.toggleAddClass();
  }

}
