import { Component } from '@angular/core';
import { IPicture } from 'models/IPicture';

@Component({
  selector: 'app-matcard-multiple-img',
  templateUrl: './matcard-multiple-img.component.html',
  styleUrls: ['./matcard-multiple-img.component.css']
})
export class MatcardMultipleImgComponent {
  pictures : Array<IPicture> = [
    {id: 4, title: 'Hiphop 1', path:'assets/dancing-882940_640.jpg', description: 'Let\'s dance. Images fromPixabay', author:'katerina1103990', alt:'Dancer image 4'},
    {id: 5, title: 'Hiphop 2', path:'assets/dance-4700009_640.jpg', description: 'Image from Pixabay', author:'Bogdan Radu', alt:'Dancer image 5'},
    {id: 6, title: 'Freedom', path:'assets/man-5635510_640.jpg', description: 'Image from Pixabay', author:'Yogendra Singh', alt:'Dancer image 6'},
  ];
}
