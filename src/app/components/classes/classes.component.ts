import { Component } from '@angular/core';

import { ClassService } from '../../services/class.service';
import { IClass } from "models/IClass";


@Component({
  selector: 'app-classes',
  templateUrl: './classes.component.html',
  styleUrls: ['./classes.component.css']
})
export class ClassesComponent {
  classes : IClass[] = [];
  iClass : IClass = 
  {
    type: '',
    style: '',
    day: '',
    reminder: false
  };

  constructor(private classService: ClassService){

  }

  ngOnInit(): void{
    this.classService.getClasses().subscribe((classes) => this.classes = classes);
    
  }

  //call classService.deleteClass first and then
  //filter and leave out from UI these items that have been deleted from server
  deleteClass(iClass: IClass){
    this.classService
      .deleteClass(iClass).subscribe(
        () => (this.classes = this.classes.filter(c => c.id !== iClass.id))
      );
  }

  toggleReminder(iClass: IClass){
    iClass.reminder = ! iClass.reminder;
    this.classService.updateReminder(iClass).subscribe();
  }

  addClass(iClass: IClass){
    //console.log("adding class works: " + iClass);
    this.classService.addClass(iClass).subscribe(
      (iClass) => this.classes.push(iClass)
    );
  }
}
