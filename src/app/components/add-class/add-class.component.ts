import { Component, Input, Output, EventEmitter } from '@angular/core';
import { IClass } from 'models/IClass';
import { Subscription } from 'rxjs';
import { UiService } from 'src/app/services/ui.service';

@Component({
  selector: 'app-add-class',
  templateUrl: './add-class.component.html',
  styleUrls: ['./add-class.component.css']
})
export class AddClassComponent {
  @Output() onAddClass : EventEmitter<IClass> = new EventEmitter();
  day: string = '';
  style: string = '';
  type: string = '';
  reminder: boolean = false;
  showAddClass: boolean = false;
  subscription!:Subscription;
  isAlert:boolean = false;
  alertMsg: string = '';

  constructor(private uiService: UiService){
    this.subscription = this.uiService
      .onToggle()
      .subscribe((value) =>(this.showAddClass = value));
  }

  showAlert(){
    this.alertMsg = 'Please add day!'
  }

  onSubmit() {
    if (!this.day) {
      this.isAlert=true;
      this.showAlert();
      return;
    } else {
      this.isAlert=false;
    }
    const newClass = {
      day: this.day,
      style: this.style,
      type: this.type,
      reminder: this.reminder
    }

    this.onAddClass.emit(newClass);

    this.day = '';
    this.style = '';
    this.type = '';
    this.reminder = false;
  }
}
