import { Component, Input, Output, EventEmitter } from '@angular/core';
import { IClass } from "models/IClass";
import { faTimes } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-class-item',
  templateUrl: './class-item.component.html',
  styleUrls: ['./class-item.component.css']
})
export class ClassItemComponent {
  @Input() iClass : IClass =  {
    type: '',
    style: '',
    day: '',
    reminder: false
  };
  @Output() onDeleteClass: EventEmitter<IClass> = new EventEmitter;
  faTimes = faTimes;
  @Output() onToggleReminder: EventEmitter<IClass> = new EventEmitter;

  onDelete(iClass: any){
    this.onDeleteClass.emit(iClass);
    
  }

  onToggle(iClass:any){
    this.onToggleReminder.emit(iClass);
  }

}
