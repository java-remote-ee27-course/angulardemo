import { Component } from '@angular/core';
import { IPicture } from 'models/IPicture';
import { CARDCONTENT } from '../../mock-content';
import { MatcardComponent } from '../matcard/matcard.component';

@Component({
  selector: 'app-matcards',
  templateUrl: './matcards.component.html',
  styleUrls: ['./matcards.component.css']
})

export class MatcardsComponent {
  matcards : IPicture[] = CARDCONTENT;
  counter = 0;

  constructor(){
    
  }
  ngOnInit():void{

  }


  //Get overall engagement (how much times all like buttons are pressed):
  shareContent() {
    this.counter++;
    console.log(this.counter);
  }
}
