import { IClass } from "models/IClass";

export const CLASSES: IClass[] = [
    {
        id:1,
        type:'technics',
        style:'flamenco',
        day:'Tuesday',
        reminder:true,
    },
    {
        id:2,
        type:'alegrias',
        style:'flamenco',
        day:'Wednesday',
        reminder:true,
    },
    {
        id:3,
        type:'tangos de triana',
        style:'flamenco',
        day:'Friday',
        reminder:false,
    },
];