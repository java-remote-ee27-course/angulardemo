export interface IClass{
    id?:number;
    type:string;
    style:string;
    day:string;
    reminder:boolean; 
}